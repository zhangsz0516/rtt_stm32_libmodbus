#include <rtthread.h>
#include <board.h>
#include "modbus.h"

#define RS485_RE GET_PIN(A, 8) //RS485控制引脚

static void mb_slave_thread(void *param)
{
    modbus_t *ctx = RT_NULL;
    int rc = 0;
    modbus_mapping_t *mb_mapping = NULL; /* 线圈与寄存器 */
    uint8_t mb_reply[MODBUS_TCP_MAX_ADU_LENGTH];

    ctx = modbus_new_rtu("/dev/uart3", 115200, 'N', 8, 1);
    modbus_rtu_set_serial_mode(ctx, MODBUS_RTU_RS485);
    modbus_rtu_set_rts(ctx, RS485_RE, MODBUS_RTU_RTS_UP);
    modbus_set_slave(ctx, 3); /* 设置从机地址 */

    mb_mapping = modbus_mapping_new(16, 16, 10, 10);
    if (mb_mapping == RT_NULL)
    {
        rt_kprintf("modbus_mapping_new failed! \n");
        modbus_free(ctx);
        return;
    }
    mb_mapping->tab_registers[0] = 'R';
    mb_mapping->tab_registers[1] = 'T';
    mb_mapping->tab_registers[2] = '-';
    mb_mapping->tab_registers[3] = 'T';
    mb_mapping->tab_registers[4] = 'h';
    mb_mapping->tab_registers[5] = 'r';
    mb_mapping->tab_registers[6] = 'e';
    mb_mapping->tab_registers[7] = 'a';
    mb_mapping->tab_registers[8] = 'd';
    modbus_connect(ctx);
    while (1)
    {
        //modbus loop
        rc = modbus_receive(ctx, mb_reply);
        if (rc > 0)
        {
            modbus_reply(ctx, mb_reply, rc, mb_mapping);
        }
        else if (rc  == -1)
        {
            /* Connection closed by the client or error */
            break;
        }
    }
    modbus_close(ctx);
    modbus_free(ctx);
}

static int rtu_test_init(void)
{
    rt_pin_mode(RS485_RE, PIN_MODE_OUTPUT);
    rt_thread_t tid;
    tid = rt_thread_create("mb_test",
                           mb_slave_thread, RT_NULL,
                           1024,
                           12, 10);
    if (tid != RT_NULL)
        rt_thread_startup(tid);

    return RT_EOK;
}

INIT_APP_EXPORT(rtu_test_init);
